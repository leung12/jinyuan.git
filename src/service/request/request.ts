import useStore from '../../store/index'
import { axios, Method } from 'taro-axios'
import Taro, { useRouter } from '@tarojs/taro'
import { uploadLogToAliyuncs } from '../../utils/index'
import { storeToRefs } from 'pinia'

/*
    路由对象
*/
const route = useRouter()

const service = axios.create({
  baseURL: process.env.HTTP_URL,
  timeout: 5000
})

// 请求拦截器
service.interceptors.request.use(
  function(config) {
    const { user } = useStore()

    config.headers['X-PROJECT'] = 'jinyuanyouxuan'
    // config.headers['X-PROJECT'] = 'dabinhaowuyanxuan'
    // config.headers['X-PROJECT'] = 'lilihaowujingxuan'
    // config.headers['X-PROJECT'] = 'xiaoshuozhenpinqijian'
    // config.headers['X-PROJECT'] = 'yayahaowujingxuan'
    config.headers['Content-Type'] = 'application/json'
    config.headers['X-CLIENT-VERSION'] = '2.7.2'
    config.headers['X-USER-TYPE'] = 'user'
    config.headers['accept'] = 'application/json'
    if (user.id) {
      config.headers['X-USER-ID'] = user.id
    }
    if (user.token) {
      config.headers['X-ACCESS-TOKEN'] = user.token
    }
    Taro.setStorageSync('appName', config.headers['X-PROJECT'])
    return config
  },
  function(error) {
    // 对请求错误做些什么
    // Taro.showToast({
    //   title: '网络异常,请检查网络链接',
    //   icon: 'none',
    //   duration: 1000
    // })
    uploadLogToAliyuncs('', '', 'error', '请求超时', `${error}`)
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  async function(response) {
    const { user, order, address } = useStore()
    const { notLoginOrderParamsList, orderListParams } = storeToRefs(order)
    const { phoneParams } = storeToRefs(user)
    const { submitConsignee } = storeToRefs(address)

    if (
      response.data.status &&
      response.data.status !== 0 &&
      response.data.status !== 10001 &&
      response.data.status !== -1 &&
      response.data.status !== 405
    ) {
      Taro.showToast({
        title: response.data.msg,
        icon: 'none',
        duration: 1000
      })
    }

    // 500: 系统繁忙
    if (response.data.status && response.data.status === 500) {
      Taro.showToast({
        title: '系统繁忙',
        icon: 'none',
        duration: 1000
      })
    }
    // 406: 下单手机格式错误
    if (response.data.status && response.data.status == 406) {
      Taro.showToast({
        title: response.data.msg,
        icon: 'none',
        duration: 1000
      })
    }
    // 10001：用户未登录
    if (response.data.status && response.data.status === 10001) {
      await user.login(route.params)
      // 登录后重新发起之前的请求(下单)
      if (notLoginOrderParamsList.value.length !== 0) {
        order.placeOrder(...notLoginOrderParamsList.value)
      }
      // 登录后重新发起之前的请求(绑定手机号)
      if (phoneParams.value.encryptedData) {
        user.bindMobileNumber(phoneParams.value.encryptedData, phoneParams.value.iv)
      }
      // 登录后重新发起之前的请求(订单列表)
      if (orderListParams.value.status) {
        order.getOrderList(orderListParams.value.status, Number(orderListParams.value.limit))
      }
      // 登录后重新发起之前的请求(提交用户地址)
      if (submitConsignee.value.address) {
        address.submitConsignees(submitConsignee.value)
      }
    }

    // status 返回不为0，上报日志
    if (response.data.status && response.data.status !== 0) {
      // 其他参数对象
      const extraObject = {
        params: response.config.params,
        response: response.data,
        data: response.config.data
      }
      uploadLogToAliyuncs(
        '',
        `${response.config.baseURL}` + `${response.config?.url}`,
        'api',
        '',
        JSON.stringify(extraObject)
      )
    }
    return response
  },
  function(error) {
    // Taro.showToast({
    //   title: '网络异常,请检查网络链接',
    //   icon: 'none',
    //   duration: 1000
    // })
    uploadLogToAliyuncs('', '', 'error', '响应超时', `${error}`)
    // 对响应错误做点什么
    return Promise.reject(error)
  }
)

interface Result<T = unknown> {
  status: number
  msg: string
  data: T
}

export const http = <T>(method: Method, url: string, submitData?: object) => {
  return service.request<Result<T>>({
    url,
    method,
    [method.toUpperCase() === 'GET' ? 'params' : 'data']: submitData
  })
}

export default service
