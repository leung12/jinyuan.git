/* 动态设置首页标题 */
import Taro from '@tarojs/taro'

let currentTitle = ''

export function setTitle() {
  const appId = Taro.getAccountInfoSync().miniProgram.appId
  if (appId === 'wx4dc88e61ae58eae2') {
    currentTitle = '金元优选'
  } else if (appId === 'wx9892c4afb7f796c6') {
    currentTitle = '大斌好物严选'
  } else if (appId === 'wx256937bc221763e0') {
    currentTitle = '莉莉好物精选'
  } else if (appId === 'wx4902c492f3dac02a') {
    currentTitle = '小硕臻品旗舰'
  }
  return currentTitle
}
