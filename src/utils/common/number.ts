import { IcommentTagObjectList } from '../../types/index'
/**
 * 根据数字获取对应的汉字
 * @param num - 数字(0-10)
 */
export function getHanByNumber(num: number) {
  const HAN_STR = '零一二三四五六七八九十'
  return HAN_STR.charAt(num)
}

/**
 * 将总秒数转换成 分：秒
 * @param seconds - 秒
 */
export function transformToTimeCountDown(seconds: number) {
  const SECONDS_A_MINUTE = 60
  function fillZero(num: number) {
    return num.toString().padStart(2, '0')
  }
  const minuteNum = Math.floor(seconds / SECONDS_A_MINUTE)
  const minute = fillZero(minuteNum)
  const second = fillZero(seconds - minuteNum * SECONDS_A_MINUTE)
  return `${minute}: ${second}`
}

/**
 * 获取指定整数范围内的随机整数
 * @param start - 开始范围
 * @param end - 结束范围
 */
export function getRandomInterger(end: number, start = 0) {
  const range = end - start
  const random = Math.floor(Math.random() * range + start)
  return random
}

/**
 * 将金额数转换  分——>元
 * @param
 */
export function centToYuan(num) {
  num = num * 0.01 //分到元
  let newNum = ''
  newNum = num.toString() //转成字符串
  const reg = newNum.indexOf('.') > -1 ? /(\d{1,3})(?=(?:\d{3})+\.)/g : /(\d{1,3})(?=(?:\d{3})+$)/g //千分符的正则
  newNum = newNum.replace(reg, '$1,') //千分位格式化
  return newNum
}

// 价格分转元方法
export function transform(data) {
  const newNum = (data * 0.01).toFixed(2)
  return newNum
}

// 优惠弹窗————去掉.00
export function conpou_transform(data) {
  const newNum = (data * 0.01).toFixed(2)
  return Number(newNum)
}

// 价格分转元方法(淘宝风格)
export function transform_taobao(str) {
  const newNumStr = (str * 0.01).toFixed(2)
  // if (newNumStr.split('.')[1] === '00') {
  //   return newNumStr.split('.')[0]
  // } else {
  //   return newNumStr
  // }
  return newNumStr
}
// 分隔字符串(淘宝)
interface Iobj {
  front: string
  after: string
}
export function splitStr_taobao(str) {
  if (str.includes('.')) {
    const obj = {} as Iobj
    obj.front = str.split('.')[0]
    obj.after = str.split('.')[1]
    return obj
  } else {
    return str
  }
}

// 时间(可传入时间戳或时间对象)， 格式，时区，默认北京时区为东8
export function timestampToTime(time, cFormat, zone = 8) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (('' + time).length === 10) time = parseInt(time) * 1000
    date = new Date(time)
  }
  // 时区调整
  const utc = time + new Date(time).getTimezoneOffset() * 60000
  const wishTime = utc + 3600000 * zone
  date = new Date(wishTime)
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const timeStr = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return timeStr
}
// 时间戳转日期(返回完整时间且时间戳为13位)
export function timestampToTime_13(timestamp) {
  const date = new Date(timestamp) //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  const Y = date.getFullYear() + '-'
  const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-'
  const D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
  const h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':'
  const m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':'
  const s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return Y + M + D + h + m + s
}

// 手机号校验
export function phoneNumberVerification(telNumber) {
  // 手机号正则
  const reg = /^(?:(?:\+|00)86)?1(?:(?:3[\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\d])|(?:9[1589]))\d{8}$/
  if (!reg.test(telNumber)) {
    return false
  } else {
    return true
  }
}

/* 倒计时 */
export function countDown(time) {
  const nowTime = +new Date()
  const inputTime = +new Date(time * 1000)
  const times = (inputTime - nowTime) / 1000
  // let d = parseInt(String(times / 60 / 60 / 24)) //剩余天数，单位秒
  // d = d < 10 ? 0 + d : d
  let h = parseInt(String((times / 60 / 60) % 24)) //小时
  h = h < 10 ? 0 + h : h
  let m = parseInt(String((times / 60) % 60)) //分
  m = m < 10 ? 0 + m : m
  let s = parseInt(String(times % 60)) //秒
  s = s < 10 ? 0 + s : s
  // return d + '天' + h + '时' + m + '分' + s + '秒'; // 天时分秒
  return h + '小时' + m + '分' + s + '秒' // 时分秒
}

/* 倒计时 5小时 */
export function countDown_fiveHours(time) {
  const nowTime = +new Date()
  const inputTime = +new Date(time)
  const times = (inputTime - nowTime) / 1000
  let h = String(parseInt(String((times / 60 / 60) % 24))) //小时
  h = Number(h) < 10 ? '0' + h : `${h}`
  let m = String(parseInt(String((times / 60) % 60))) //分
  m = Number(m) < 10 ? '0' + m : `${m}`
  let s = String(parseInt(String(times % 60))) //秒
  s = Number(s) < 10 ? '0' + s : `${s}`
  return {
    h: h,
    m: m,
    s: s
  }
}

/* 5分钟倒计时 */
export function fiveMinutecountDown(time) {
  const nowTime = +new Date()
  const inputTime = +new Date(time) + 300000
  const times = (inputTime - nowTime) / 1000
  // let d = parseInt(String(times / 60 / 60 / 24)) //剩余天数，单位秒
  // d = d < 10 ? 0 + d : d
  let h = String(parseInt(String((times / 60 / 60) % 24))) //小时
  h = Number(h) < 10 ? '0' + h : `${h}`
  let m = String(parseInt(String((times / 60) % 60))) //分
  m = Number(m) < 10 ? '0' + m : `${m}`
  let s = String(parseInt(String(times % 60))) //秒
  s = Number(s) < 10 ? '0' + s : `${s}`
  return {
    h: h,
    m: m,
    s: s
  }
}

/* 未支付弹窗的倒计时 */
export function notPayCountDown(time) {
  const nowTime = +new Date()
  const inputTime = +new Date(time * 1000)
  const times = (inputTime - nowTime) / 1000

  let h = String(parseInt(String((times / 60 / 60) % 24))) //小时
  h = Number(h) < 10 ? '0' + h : `${h}`
  let m = String(parseInt(String((times / 60) % 60))) //分
  m = Number(m) < 10 ? '0' + m : `${m}`
  let s = String(parseInt(String(times % 60))) //秒
  s = Number(s) < 10 ? '0' + s : `${s}`
  return {
    h: h,
    m: m,
    s: s
  }
}

// 订单列表倒计时
export function orderCountDown(time) {
  const nowTime = +new Date()
  const inputTime = +new Date(time * 1000)
  const times = (inputTime - nowTime) / 1000

  let h = String(parseInt(String((times / 60 / 60) % 24))) //小时
  h = Number(h) < 10 ? '0' + h : `${h}`
  let m = String(parseInt(String((times / 60) % 60))) //分
  m = Number(m) < 10 ? '0' + m : `${m}`
  let s = String(parseInt(String(times % 60))) //秒
  s = Number(s) < 10 ? '0' + s : `${s}`
  return {
    h: h,
    m: m,
    s: s
  }
}

// 对象转为数组对象
export function objectChangeArrayObj(values) {
  // console.log('values', values)
  const arr = [] as IcommentTagObjectList[]
  let obj = {}
  obj = values.reduce((prev, next) => {
    prev[next] = prev[next] + 1 || 1
    return prev
  }, {})
  for (const item in obj) {
    arr.push({
      tagName: item,
      count: obj[item]
    })
  }
  return arr
}

/*
  获取一个两数之间的随机整数，包括两个数在内
*/
export function getRandomIntInclusive(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min //含最大值，含最小值
}
