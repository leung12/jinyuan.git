/*
    小程序主体,店铺信息
*/
import Taro from '@tarojs/taro'
import { reactive } from 'vue'

const shopInfo = reactive({
  shopName: '',
  companyName: '',
  logo: '',
  extInfo: '',
  corpId: ''
})

const appId = Taro.getAccountInfoSync().miniProgram.appId

export function handleGetShopInfo() {
  if (appId === 'wx4dc88e61ae58eae2') {
    shopInfo.shopName = '金元优选'
    shopInfo.companyName = '广州金元盛世网络科技有限公司'
    shopInfo.logo =
      'http://jy-public-static.oss-cn-hangzhou.aliyuncs.com/dev-resource/images/coupon/jinyuanyouxuan.png'
    shopInfo.extInfo = 'https://work.weixin.qq.com/kfid/kfcc7a5899533d7faf7'
    shopInfo.corpId = 'ww816d71a83d0b7ff9'
  } else if (appId === 'wx9892c4afb7f796c6') {
    shopInfo.shopName = '大斌好物严选'
    shopInfo.companyName = '经济技术开发区俸铭四合食品店'
    shopInfo.logo =
      'https://jy-public-static.oss-cn-hangzhou.aliyuncs.com/dev-resource/images/coupon/dabinhaowu.webp'
    shopInfo.extInfo = 'https://work.weixin.qq.com/kfid/kfc624f8e41208d3cbb'
    shopInfo.corpId = 'ww1d6c06d653df2cc6'
  } else if (appId === 'wx256937bc221763e0') {
    shopInfo.shopName = '莉莉好物精选'
    shopInfo.companyName = '官渡区亚安贸易商行'
    shopInfo.logo =
      'https://jy-public-static.oss-cn-hangzhou.aliyuncs.com/dev-resource/images/lilihaowu-logo.png'
    shopInfo.extInfo = 'https://work.weixin.qq.com/kfid/kfcbddb5e90fb3865c2'
    shopInfo.corpId = 'wwde755e77b4ac5fee'
  } else if (appId === 'wx4902c492f3dac02a') {
    shopInfo.shopName = '小硕臻品旗舰'
    shopInfo.companyName = '新邵县坪上镇子硕食品店'
    shopInfo.logo =
      'http://jy-public-static.oss-cn-hangzhou.aliyuncs.com/dev-resource/images/coupon/avatar.png'
    shopInfo.extInfo = 'https://work.weixin.qq.com/kfid/kfc9aebfa285f90d44d'
    shopInfo.corpId = 'ww95189b50c625e7ef'
  } else if (appId === 'wx001c35783ea5bbc6') {
    shopInfo.shopName = '雅雅好物精选'
    shopInfo.companyName = '安吉偲唯百货店'
    shopInfo.logo =
      'https://jy-public-static.oss-cn-hangzhou.aliyuncs.com/dev-resource/images/yayahaowu-logo.png'
    shopInfo.extInfo = ''
    shopInfo.corpId = ''
  }

  return shopInfo
}
