/*
  对 chooseLocation api 方法返回参数处理
*/
export function formatLocation(res) {
  let regex = /^(北京市|天津市|重庆市|上海市|香港特别行政区|澳门特别行政区)/
  let region_province = [] as any
  const addressBean = {
    region_province: '',
    region_country: '',
    region_city: '',
    address: ''
  }

  function regexAddressBean(address, addressBean) {
    regex = /^(.*?[市]|.*?地区|.*?特别行政区)(.*?[市区县])(.*?)$/g
    const addxress: any = regex.exec(address)
    addressBean.region_city = addxress[1]
    addressBean.region_country = addxress[2]
    addressBean.address = addxress[3] + '(' + res.name + ')'
    // console.log(addxress)
  }
  if (!(region_province = regex.exec(res.address))) {
    regex = /^(.*?(省|自治区))(.*?)$/
    region_province = regex.exec(res.address)
    addressBean.region_province = region_province[1]
    regexAddressBean(region_province[3], addressBean)
  } else {
    addressBean.region_province = region_province[1]
    regexAddressBean(res.address, addressBean)
  }

  return addressBean
}
