/*
    消息订阅 tmplIds
*/
import Taro from '@tarojs/taro'

let tmplIds = [] as string[]
const appId = Taro.getAccountInfoSync().miniProgram.appId

if (appId === 'wx4dc88e61ae58eae2') {
  tmplIds = [
    'eIJ2xhMLZQacQQwVQyV7OOK_qeAikQJlL0hg8RW0L1Y',
    'sNtirD0qGn4JEbwr7PA_BeaENvfS8FSQXeCAbKGXqn4',
    'L5DSTWRr-kqTi1AfHv3gCZ44aSx7p4OkHqEX8VjATMQ'
  ]
} else if (appId === 'wx9892c4afb7f796c6') {
  tmplIds = [
    'sqhzqWHsT7pNJytOHmUGsYh53N3wPACsHVHntbJMaa8',
    'tuK48-6h2vd2YacmuHIdp6tpA3dcmXs3zvrKvl2kT00',
    'i6wd3pd7Iaks-qnOgjHjw0VgqqPuH-SIhdThx3vH_6Y'
  ]
} else if (appId === 'wx9ca1ad081f2d3d43') {
  tmplIds = [
    'CXznESEMFSnI_x49m903CNOff-P-5nWjP58BLuOEnHU',
    'FWDVqv4y3492Wkx3-E1X604leyvvvrWwWtSpxDW-CZE',
    'CFk2iP03R-AhK064H6IAGgXIrQ090uY2giRTXC6YEyg'
  ]
}

export default tmplIds
