/* 阿里云日志采集 */

// 导入阿里云依赖模块
import SlsTracker from '@aliyun-sls/web-track-mini'
import Taro from '@tarojs/taro'

interface Irouteparams {
  $taroTimestamp?: number
  utm_source?: string
  utm_medium?: string
  utm_content?: string
  adId?: string
  clickId?: string
  code?: string
  affId?: string
}

export function uploadLogToAliyuncs(
  routeparams: Irouteparams | any,
  path: string,
  eventType: string,
  clickObjectContent?: string,
  extra?: any
) {
  // 1.配置opts参数
  const opts = {
    host: 'cn-hangzhou.log.aliyuncs.com', // 所在地域的服务入口。例如cn-hangzhou.log.aliyuncs.com
    project: 'jinyuan-mall-web-tracking', // Project名称。
    logstore: 'frontend', // Logstore名称。
    time: 20, // 发送日志的时间间隔，默认是10秒。
    count: 10, // 发送日志的数量大小，默认是10条。
    topic: '日志', // 自定义日志主题。
    source: 'applet', // 自定义日志来源
    tags: {
      tags: ''
    } // 自定义日志标签
  }
  // 2.创建SlsTracker对象(通过该对象向日志服务发起上传日志请求)
  const tracker = new SlsTracker(opts)
  // 3.上传日志(此处为上传日志的具体业务逻辑，您可以在此定义希望采集的日志详细情况)
  tracker.send({
    event: eventType,
    title: clickObjectContent ? clickObjectContent : '',
    url: path ? path : '',
    uid: Taro.getStorageSync('userId') ? Taro.getStorageSync('userId') : '',
    clientVersion: Taro.getAccountInfoSync().miniProgram.version
      ? Taro.getAccountInfoSync().miniProgram.version
      : '',
    appName: Taro.getStorageSync('appName') ? Taro.getStorageSync('appName') : '',
    utm_source: routeparams.utm_source ? routeparams.utm_source : '',
    utm_medium: routeparams.utm_medium ? routeparams.utm_medium : '',
    utm_content: routeparams.utm_content ? routeparams.utm_content : '',
    adId: routeparams.adId ? routeparams.adId : extra?.ksUnitId ? extra?.ksUnitId : '',
    adAccount: extra?.ksSiteAccountId ? extra?.ksSiteAccountId : '',
    clickId: routeparams.clickId ? routeparams.clickId : '',
    productCode: routeparams.code ? routeparams.code : '',
    affId: routeparams.affId ? routeparams.affId : '',
    scene: Taro.getStorageSync('scene') ? Taro.getStorageSync('scene') : '',
    extra: extra ? extra : ''
  })
  // console.log('日志', tracker)
}
