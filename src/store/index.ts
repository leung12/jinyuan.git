import useProductStore from './modules/product'
import useOrderStore from './modules/order'
import useUserStore from './modules/user'
import useAddressStore from './modules/address'
import useHomeStore from './modules/home'
import useFeedbackStore from './modules/feedback'

// pinia各状态统一管理
const useStore = () => {
  return {
    product: useProductStore(),
    order: useOrderStore(),
    user: useUserStore(),
    address: useAddressStore(),
    home: useHomeStore(),
    feedback: useFeedbackStore()
  }
}

export default useStore
