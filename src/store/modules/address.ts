import { defineStore } from 'pinia'
import Taro from '@tarojs/taro'
// import provincialAndUrbanAreasList from '../../utils/common/regionalData'
import { IaddressObject, Iprovince, Iconsignee, Icity } from '../../types/index'
import { http } from '../../service/request/request'

const app = Taro.getApp()

const useAddressStore = defineStore('address', {
  state: () => ({
    addressObject: {} as IaddressObject, // 微信地址本返回地址对象
    isSelect: false,
    consignee: {} as Iconsignee,
    wechatProviceNameObject: {} as Iprovince,
    wechatCityNameObject: {} as Icity,
    submitConsignee: {} as Iconsignee // 用户处理 10001 响应码
  }),
  getters: {},
  actions: {
    // 选择微信地址
    async chooseAddress(source: string, id?: string) {
      try {
        const res = await Taro.chooseAddress()
        console.log('微信地址', res)
        this.isSelect = true
        this.addressObject = res

        if (id) {
          this.consignee.provinceName = res.provinceName
          this.consignee.cityName = res.cityName
          this.consignee.districtName = res.countyName
          this.consignee.name = res.userName
          this.consignee.mobile = res.telNumber
          this.consignee.address = res.detailInfo

          this.modifyAddress(id, this.consignee)
        }

        app.uma.trackEvent('select_address', { select: '选择地址', source })
      } catch (error) {
        this.isSelect = false
        app.uma.trackEvent('select_address', { select: '没有选择' })
      }
    },
    // 提交地址
    async submitConsignees(consignee) {
      try {
        this.submitConsignee = consignee
        const result = await http('post', '/user/consignees', {
          name: consignee.name,
          mobile: consignee.mobile,
          provinceId: consignee.provinceId ? consignee.provinceId : '',
          provinceName: consignee.provinceName,
          cityId: consignee.cityId ? consignee.cityId : '',
          cityName: consignee.cityName,
          districtId: consignee.districtId ? consignee.districtId : '',
          districtName: consignee.districtName,
          address: consignee.address
        })
        console.log('提交用户地址', result)
        if (result.data.status === 0) {
          this.submitConsignee = {}
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 修改地址
    async modifyAddress(id: string, consignee: Iconsignee) {
      try {
        const result = await http('PUT', `/orders/${id}`, { consignee })
        console.log('修改地址', result)
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    }
  }
})

export default useAddressStore
