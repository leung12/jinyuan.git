import { defineStore } from 'pinia'
import Taro from '@tarojs/taro'
import {
  IloginInfo,
  IuserProfile,
  ImodifyUseInfo,
  IbindPhone,
  IutmInfoparams,
  InotPayOrderList,
  ImyMall
} from '../../types/index'
import { http } from '../../service/request/request'
const useUserStore = defineStore('user', {
  state: () => ({
    userInfo: {} as IuserProfile, // 微信用户信息

    token: Taro.getStorageSync('accessToken') || '',
    id: Taro.getStorageSync('userId') || '',
    mobile: Taro.getStorageSync('userPhone') || '',
    nickName: Taro.getStorageSync('userNickName') || '',
    avatar: Taro.getStorageSync('userAvatar') || '',
    userProvince: Taro.getStorageSync('userProvince') || '',
    userCity: Taro.getStorageSync('userCity') || '',

    // 手机号码参数
    phoneParams: {
      encryptedData: '',
      iv: ''
    },

    notPayOrderList: [] as InotPayOrderList, // 未支付订单列表数据
    myUserInfo: {} as ImyMall
  }),
  getters: {},
  actions: {
    // 获取微信用户信息
    async getUserProfile() {
      try {
        const result = await Taro.getUserProfile({
          desc: '用于完善会员信息'
        })
        if (result.errMsg === 'getUserProfile:ok') {
          this.userInfo = result.userInfo
          // 调用修改用户信息接口
          this.modifyUserInfo(result.userInfo.nickName, result.userInfo.avatarUrl)
        }
      } catch (error) {
        Taro.showToast({
          title: '获取失败',
          icon: 'error',
          duration: 1000
        })
      }
    },
    // 登录
    async login(params: IutmInfoparams) {
      try {
        const { utm_source, utm_medium, utm_content, clickId } = params
        const scene = Taro.getStorageSync('scene')

        const res = await Taro.login()
        const result = await http<IloginInfo>('POST', '/user/login', {
          jsCode: res.code,
          utmInfo: {
            utm_source: utm_source ? utm_source : '',
            utm_medium: utm_medium ? utm_medium : '',
            utm_content: utm_content ? utm_content : '',
            scene,
            clickId: clickId ? Number(clickId) : ''
          }
        })
        if (result.data.status === 0) {
          this.token = result.data.data.accessToken
          this.id = result.data.data.id
          this.mobile = result.data.data.mobile
          this.nickName = result.data.data.nickName
          this.avatar = result.data.data.avatar
          this.userProvince = result.data.data.province
          this.userCity = result.data.data.city
          Taro.setStorageSync('accessToken', result.data.data.accessToken)
          Taro.setStorageSync('userId', result.data.data.id)
          Taro.setStorageSync('userPhone', result.data.data.mobile)
          Taro.setStorageSync('userNickName', result.data.data.nickName)
          Taro.setStorageSync('userAvatar', result.data.data.avatar)
          Taro.setStorageSync('userProvince', result.data.data.province)
          Taro.setStorageSync('userCity', result.data.data.city)
        }
        console.log('登录', result)
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 修改用户信息
    async modifyUserInfo(nickName: string, avatar: string) {
      try {
        const result = await http<ImodifyUseInfo>('PUT', '/user/modify-info', {
          nickName,
          avatar
        })
        console.log('修改用户信息', result)
        if (result.data.status === 0) {
          Taro.showToast({
            title: '绑定用户信息',
            icon: 'success',
            duration: 1000
          })
          this.nickName = result.data.data.nickName
          this.avatar = result.data.data.avatar
          Taro.setStorageSync('userAvatar', result.data.data.avatar)
          Taro.setStorageSync('userNickName', result.data.data.nickName)
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 绑定手机号码
    async bindMobileNumber(encryptedData: string, iv: string) {
      // 记录绑定手机号所需参数，用于处理10001错误码
      this.phoneParams.encryptedData = encryptedData
      this.phoneParams.iv = iv

      const res = await Taro.login()
      const result = await http<IbindPhone>('POST', '/user/bind-mobile', {
        jsCode: res.code,
        encryptedData,
        iv
      })
      console.log('绑定手机号', result)
      if (result.data.status === 0) {
        this.mobile = result.data.data.mobile
        Taro.setStorageSync('userPhone', result.data.data.mobile)
        Taro.showToast({
          title: '绑定手机号',
          icon: 'success',
          duration: 1000
        })

        this.phoneParams = {
          encryptedData: '',
          iv: ''
        }
      } else {
        Taro.showToast({
          title: '绑定手机号失败',
          icon: 'none',
          duration: 1000
        })
      }
    },
    // 获取我的页面用户信息
    async getUserInfo() {
      const result = await http<ImyMall>('GET', '/user/my-mall', {
        fields:
          '{user{id,nickName,avatar,mobile},weworkCsUrl,weworkCorpId,notPayOrders{id,createTime,expireTime,payTime,productAmount,payAmount,discountAmount,status,items,shippingFee,transactionNo,consignee,shippingName,shippingTime,trackingNumber},store}'
      })
      console.log('我的页面用户信息', result)
      if (result.data.status === 0) {
        this.notPayOrderList = result.data.data.notPayOrders
        this.myUserInfo = result.data.data

        this.mobile = result.data.data.user.mobile
        Taro.setStorageSync('userPhone', result.data.data.user.mobile)
        this.nickName = result.data.data.user.nickName
        Taro.setStorageSync('userNickName', result.data.data.user.nickName)
        this.avatar = result.data.data.user.avatar
        Taro.setStorageSync('userAvatar', result.data.data.user.avatar)
        this.id = result.data.data.user.id
        Taro.setStorageSync('userId', result.data.data.user.id)
      }
    }
  }
})

export default useUserStore
