import { defineStore } from 'pinia'

const useFeedbackStore = defineStore('feedback', {
  state: () => ({}),
  getters: {},
  actions: {}
})

export default useFeedbackStore
