import { defineStore } from 'pinia'
import { http } from '../../service/request/request'
import { Iresource, ItabsObject, ItabsrArray, IrandProduct } from '../../types/index'

const useHomeStore = defineStore('home', {
  state: () => ({
    tabsObject: {} as ItabsObject, // 首页tab数据
    tabsList: [] as ItabsrArray, // tab列表
    randProductList: [] as IrandProduct // 首页轮播随机商品图列表
  }),
  getters: {},
  actions: {
    // 获取首页资源位
    async getResources() {
      const result = await http<Iresource>('GET', '/resource-slot/home')
      console.log('首页资源位', result)
      if (result.data.status === 0) {
        this.swiperList = result.data.data[0].items
        this.tabsObject = result.data.data[1]
        this.tabsList = result.data.data[1].items
        this.defaultValue = result.data.data[1].items[0].value
      }
    },
    // 获取首页轮播随机商品图
    async getrandProduct() {
      const result = await http<IrandProduct>('GET', '/resource-slot/code/rand-product', {
        fields: '{id,code,name,cover}',
        limit: '6'
      })
      if (result.data.status === 0) {
        this.randProductList = result.data.data
      }
      // console.log('首页轮播随机商品图片', result)
    }
  }
})

export default useHomeStore
