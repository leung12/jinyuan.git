import { defineStore } from 'pinia'
import { http } from '../../service/request/request'
import Taro from '@tarojs/taro'
import {
  IadvertisementParams,
  IIdiscountPromptArray,
  IorderdiscountPrompt,
  Iconsignee,
  IbackhaulParams,
  IstoreQrCode
} from '../../types/index'

const useProductStore = defineStore('product', {
  state: () => ({
    productUtmInfo: {} as IadvertisementParams, // 广告相关参数
    kuaishouBackhaulParams: {} as IbackhaulParams, // 快手回传归因参数

    // 商品详情、评论、问大家、问大家详情页面共享优惠数据部分 ------------
    notPayCouponObject: {} as any,
    userIsPlaceOrder: false, // 记录用户下单
    discountPrompt: [] as IIdiscountPromptArray, // 最终要提交的优惠数据列表
    discountPromptObj: {} as IorderdiscountPrompt, // 百分比优惠
    entryCouponPromptObj: {} as IorderdiscountPrompt, // 礼金优惠
    consignee: {} as Iconsignee,
    entryCoupon: 0,
    leaveDiscount: 0,
    userKnowTotalCoupon: false,
    userClickCount: 0,
    userKnowLeave1Coupon: false,
    userKnowLeaveNotReceiveCoupon: false,
    userKnowOrderNoAddressCoupon: false,
    userKnowOrderFillAddressCoupon: false,
    userKnowEntryCoupon: false,
    recordIsReceiveEntryCoupon: false, // 记录是否领取礼金
    firstDiscountIsReceive: false, // 记录首次优惠是否领取

    storeId: 0, // 店铺 id
    storeId_ad: 0, // 广告进来的店铺 id
    storeQrCodeObject: {} as IstoreQrCode,
    userFromAd: false, // 记录用户是否广告进入
    productId: 0, //商品id
    isHaveRecommandProduct: false // 记录当前商品是否有关联商品
  }),
  getters: {},
  actions: {
    // 清理优惠数据
    clearPreferentialData() {
      this.userClickCount = 0
      this.consignee = {}
      this.firstDiscountIsReceive = false
      this.userIsPlaceOrder = false
      this.userKnowTotalCoupon = false
      this.userKnowEntryCoupon = false
    },

    // 问大家页面提问
    async productQuestions(code: string, content) {
      try {
        const result = await http('POST', `/products/${code}/asks`, { content })
        console.log('商品提问', result)
        if (result.data.status === 0) {
          Taro.showToast({
            title: '提问成功,审核通过后将展示内容',
            icon: 'none',
            duration: 2000
          })
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 上传快手参数
    async postKsAdParams(code: string, ksUtmInfo: IadvertisementParams) {
      const result = await http('POST', `/products/${code}/events`, {
        utm_source: ksUtmInfo.utm_source ? ksUtmInfo.utm_source : '',
        utm_medium: ksUtmInfo.utm_medium ? ksUtmInfo.utm_medium : '',
        utm_content: ksUtmInfo.utm_content ? ksUtmInfo.utm_content : '',
        clickId: ksUtmInfo.clickId ? ksUtmInfo.clickId : '',
        scene: Taro.getStorageSync('scene') ? Taro.getStorageSync('scene') : '',
        traceId: ksUtmInfo.traceId ? ksUtmInfo.traceId : '',
        data: ksUtmInfo.data
      })
      console.log('商品上报', result)
    },
    // 获取店铺二维码
    async getStoreQrCode(id) {
      const result = await http<IstoreQrCode>('GET', `/store/${id}`, {
        fields: '{id,randomQrcodeImage,qrcodeDescription}'
      })
      console.log('店铺二维码', result)
      if (result.data.status === 0) {
        this.storeQrCodeObject = result.data.data
      }
    }
  }
})

export default useProductStore
