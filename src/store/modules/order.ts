import { defineStore } from 'pinia'
import { http } from '../../service/request/request'

import Taro from '@tarojs/taro'
import {
  IorderInfo,
  IorderList,
  IorderDetailObject,
  IitemObject,
  IadvertisementParams,
  Iconsignee,
  IpriceInfo,
  IcommentObject
} from '../../types/index'

const app = Taro.getApp()

const useOrderStore = defineStore('order', {
  state: () => ({
    // orderId: '', // 此次下单的订单id,用于再下单
    advertisementParams: {} as IadvertisementParams, // 广告点击相关参数
    userConsigneeInfoObject: {} as Iconsignee, // 用户下单的地址信息

    orderDetailObject: {} as IorderDetailObject, // 完整订单详情
    orderDetail: {} as IitemObject, // 订单的商品详情

    entryConponObj: {} as IpriceInfo, // 新人红包优惠对象
    limitTimeCouponObj: {} as IpriceInfo, // 限时减免优惠对象
    orderOriginPrice: 0,
    limitedTimeOffer: 0,

    orderListParams: {
      status: '',
      limit: 0
    }, // 记录点击的状态订单列表

    tmplIds: [], // 订阅消息id数组

    notLoginOrderParamsList: [], // 未登录发起订单的所需参数列表
    notPayDiscount: 0 as number,

    // 订单新消息红点提示相关数据
    notPaid_redDot: false,
    toShipping_redDot: false,
    toReceive_redDot: false,
    toComment_redDot: false,
    afterSale_redDot: false,
    finished_redDot: false,
    currentStatus: '',

    chooseId: ''
  }),
  getters: {},
  actions: {
    // 下订单
    async placeOrder(...params) {
      console.log('下单参数', params)

      // 保存用户的收货/地址信息
      this.userConsigneeInfoObject = params[4]

      // 假如用户未登录点击下单，保存订单接口所需参数，用于登录后再提交
      this.notLoginOrderParamsList = params
      // this.notLoginOrderParamsList[3] = this.advertisementParams

      const result = await http<IorderInfo>('POST', '/orders', {
        productId: params[0] ? params[0] : '',
        productSpec: params[1] ? params[1] : '',
        quantity: params[2] ? params[2] : '',
        utmInfo: {
          utm_source: params[3].utm_source ? params[3].utm_source : '',
          utm_medium: params[3].utm_medium ? params[3].utm_medium : '',
          utm_content: params[3].utm_content ? params[3].utm_content : '',
          utm_campaign: params[3].utm_campaign ? params[3].utm_campaign : '',
          clickId: params[3].clickId ? Number(params[3].clickId) : '',
          scene: Taro.getStorageSync('scene') ? Taro.getStorageSync('scene') : '',
          traceId: params[3].traceId ? params[3].traceId : '',
          data: params[3].data
        },
        consignee: params[4] ? params[4] : '',
        buyerNote: params[5] ? params[5] : '',
        couponId: 0,
        payType: 'wechatpay',
        discountPrompt: params[6]
      })

      console.log('用户下单', result)
      if (result.data.status === 0) {
        this.tmplIds = result.data.data.subscribeMsgTplIds

        Taro.requestPayment({
          timeStamp: result.data.data.miniPaymentParams.timeStamp,
          nonceStr: result.data.data.miniPaymentParams.nonceStr,
          package: result.data.data.miniPaymentParams.package,
          signType: result.data.data.miniPaymentParams.signType,
          paySign: result.data.data.miniPaymentParams.paySign,
          success: function(res) {
            console.log('支付成功', res)
            Taro.showToast({
              title: '支付成功',
              icon: 'success',
              duration: 1000
            })
            Taro.requestSubscribeMessage({
              tmplIds: result.data.data.subscribeMsgTplIds,
              success: function() {
                Taro.reLaunch({
                  url: `/packageC/paymentSucceeded/paymentSucceeded`
                })
              },
              fail: function() {
                Taro.reLaunch({
                  url: `/packageC/paymentSucceeded/paymentSucceeded`
                })
              }
            })

            // 友盟 SDK 埋点
            if (params[6].length) {
              params[6].forEach(item => {
                console.log('payment123123')
                if (item.type === 'enter') {
                  app.uma.trackEvent('payment', {
                    paymentStatus: '支付成功',
                    source: params[7],
                    type: `${item.type}`
                  })
                } else {
                  app.uma.trackEvent('payment', {
                    paymentStatus: '支付成功',
                    source: params[7],
                    type: `${item.type}`
                  })
                }
              })
            } else {
              app.uma.trackEvent('payment', {
                paymentStatus: '支付成功',
                source: params[7],
                type: 'null'
              })
            }
          },
          fail: function(res) {
            console.log('支付失败', res)
            Taro.requestSubscribeMessage({
              tmplIds: result.data.data.notPaySubscribeMsgTplIds,
              success: function() {
                Taro.navigateTo({
                  url: `/packageC/orderDetail/orderDetail?showPop=show&id=${
                    result.data.data.orderId
                  }&status=待支付&leaveDiscount=${
                    params[6]
                  }&useNotPayDiscountButNobuy=${true}}&orderFail=true`,
                  success(res: any) {
                    res.eventChannel.emit('notPayCouponObject', params[8])
                  }
                })
              },
              fail: function() {
                Taro.navigateTo({
                  url: `/packageC/orderDetail/orderDetail?showPop=show&id=${
                    result.data.data.orderId
                  }&status=待支付&leaveDiscount=${
                    params[6]
                  }&useNotPayDiscountButNobuy=${true}}&orderFail=true`,
                  success(res: any) {
                    res.eventChannel.emit('notPayCouponObject', params[8])
                  }
                })
              }
            })
            // 友盟 SDK 埋点
            if (params[6].length) {
              params[6].forEach(item => {
                if (item.type === 'enter') {
                  app.uma.trackEvent('payment', {
                    paymentStatus: '支付失败',
                    source: params[7],
                    type: `${item.type}`
                  })
                } else {
                  app.uma.trackEvent('payment', {
                    paymentStatus: '支付失败',
                    source: params[7],
                    type: `${item.type}`
                  })
                }
              })
            } else {
              app.uma.trackEvent('payment', {
                paymentStatus: '支付失败',
                source: params[7],
                type: 'null'
              })
            }
          }
        })
        this.notLoginOrderParamsList = null
      }
    },
    // 修改订单
    async placeOrderUseNotPayDiscount(id: string, notPayDiscount: number) {
      try {
        const result = await http<IorderInfo>('PUT', `/orders/${id}`, {
          notPayDiscount
        })
        console.log('修改订单', result)
        if (result.data.status === 0) {
          Taro.requestPayment({
            timeStamp: result.data.data.miniPaymentParams.timeStamp,
            nonceStr: result.data.data.miniPaymentParams.nonceStr,
            package: result.data.data.miniPaymentParams.package,
            signType: result.data.data.miniPaymentParams.signType,
            paySign: result.data.data.miniPaymentParams.paySign,
            success: function(res) {
              console.log('未支付弹窗支付成功', res)
              Taro.showToast({
                title: '支付成功',
                icon: 'success',
                duration: 1000
              })
              Taro.requestSubscribeMessage({
                tmplIds: result.data.data.subscribeMsgTplIds,
                success: function() {
                  Taro.reLaunch({
                    url: `/packageC/paymentSucceeded/paymentSucceeded`
                  })
                },
                fail: function() {
                  Taro.reLaunch({
                    url: `/packageC/paymentSucceeded/paymentSucceeded`
                  })
                }
              })

              app.uma.trackEvent('payment', {
                paymentStatus: '支付成功',
                source: '订单详情挽留优惠弹窗',
                type: 'notPay'
              })
            },
            fail: function(res) {
              console.log('未支付弹窗取消支付', res)
              Taro.requestSubscribeMessage({
                tmplIds: result.data.data.notPaySubscribeMsgTplIds,
                success: function() {
                  Taro.redirectTo({
                    url: `/packageC/orderDetail/orderDetail?id=${
                      result.data.data.orderId
                    }&status=待支付&useNotPayDiscountButNobuy=${true}`
                  })
                },
                fail: function() {
                  Taro.redirectTo({
                    url: `/packageC/orderDetail/orderDetail?id=${
                      result.data.data.orderId
                    }&status=待支付&useNotPayDiscountButNobuy=${true}`
                  })
                }
              })

              app.uma.trackEvent('payment', {
                paymentStatus: '支付失败',
                source: '订单详情挽留优惠弹窗',
                type: 'notPay'
              })
            }
          })
        }

        this.notPayDiscount = notPayDiscount
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 获取用户订单列表(仅获取订单 id)
    async getOrderList_id(status: string) {
      // 记录用户点哪个状态订单列表，用于处理10001状态错误码
      this.orderListParams.status = status
      this.orderListParams.limit = 1
      const result = await http<IorderList>('GET', '/orders', {
        fields: '{id}',
        page: '1',
        limit: 1,
        status
      })
      // console.log(`订单列表(${status})`, result)
      if (result.data.status === 0) {
        if (status === 'NOT_PAID' && result.data.data.length) {
          if (Taro.getStorageSync('NOT_PAID_ID') !== result.data.data[0].id) {
            this.notPaid_redDot = true
            Taro.setStorageSync('NOT_PAID_ID', result.data.data[0].id)

            if (this.currentStatus === 'NOT_PAID') {
              this.notPaid_redDot = false
            }
          }
        }
        if (status === 'TO_SHIPPING' && result.data.data.length) {
          if (Taro.getStorageSync('TO_SHIPPING_ID') !== result.data.data[0].id) {
            this.toShipping_redDot = true
            Taro.setStorageSync('TO_SHIPPING_ID', result.data.data[0].id)

            if (this.currentStatus === 'TO_SHIPPING') {
              this.toShipping_redDot = false
            }
          }
        }
        if (status === 'TO_RECEIVE' && result.data.data.length) {
          if (Taro.getStorageSync('TO_RECEIVE_ID') !== result.data.data[0].id) {
            this.toReceive_redDot = true
            Taro.setStorageSync('TO_RECEIVE_ID', result.data.data[0].id)

            if (this.currentStatus === 'TO_RECEIVE') {
              this.toReceive_redDot = false
            }
          }
        }
        if (status === 'TO_COMMENT' && result.data.data.length) {
          if (Taro.getStorageSync('TO_COMMENT_ID') !== result.data.data[0].id) {
            this.toComment_redDot = true
            Taro.setStorageSync('TO_COMMENT_ID', result.data.data[0].id)

            if (this.currentStatus === 'TO_COMMENT') {
              this.toComment_redDot = false
            }
          }
        }
        if (status === 'AFTER_SALE' && result.data.data.length) {
          if (Taro.getStorageSync('AFTER_SALE_ID') !== result.data.data[0].id) {
            this.afterSale_redDot = true
            Taro.setStorageSync('AFTER_SALE_ID', result.data.data[0].id)
          }
        }
        if (status === 'FINISHED' && result.data.data.length) {
          if (Taro.getStorageSync('FINISHED_ID') !== result.data.data[0].id) {
            this.finished_redDot = true
            Taro.setStorageSync('FINISHED_ID', result.data.data[0].id)

            if (this.currentStatus === 'FINISHED') {
              this.finished_redDot = false
            }
          }
        }
        this.orderListParams = {
          status: '',
          limit: ''
        }
      }
    },
    // 获取订单详情
    async getOrderDetail(id: string) {
      try {
        const result = await http<IorderDetailObject>('GET', `/orders/${id}`, {
          fields:
            '{id,createTime,payTime,productAmount,payAmount,discountAmount,status,items,shippingFee,transactionNo,consignee,shippingName,shippingTime,trackingNumber,actions,store,priceInfo}',
          id
        })
        console.log('订单详情', result)
        if (result.data.status === 0) {
          this.orderDetailObject = result.data.data
          this.orderDetail = result.data.data.items[0]

          // 筛选新人红包优惠对象
          this.entryConponObj = result.data.data.priceInfo.find(item => {
            return item.title === '新人红包'
          })
          this.limitTimeCouponObj = result.data.data.priceInfo.find(item => {
            return item.title === '限时减免'
          })

          if (result.data.data.items[0].originPrice) {
            this.orderOriginPrice = result.data.data.items[0].originPrice
          } else {
            this.orderOriginPrice = 0
          }

          // 没有划线价、没有优惠
          if (this.orderOriginPrice === 0 && this.limitTimeCouponObj === undefined) {
            this.limitedTimeOffer = 0
          }
          // 没有划线价，有优惠
          else if (this.orderOriginPrice === 0 && this.limitTimeCouponObj) {
            this.limitedTimeOffer = this.limitTimeCouponObj.amount
          }
          // 有划线价，没有优惠
          else if (this.orderOriginPrice && this.limitTimeCouponObj === undefined) {
            this.limitedTimeOffer =
              result.data.data.items[0].originPrice - result.data.data.items[0].price
          }
          // 有划线价、有优惠
          else if (this.orderOriginPrice && this.limitTimeCouponObj) {
            this.limitedTimeOffer =
              result.data.data.items[0].originPrice -
              result.data.data.items[0].price +
              this.limitTimeCouponObj.amount
          }
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 订单列表页里再次支付
    async paymentAgain(id: string, source: string) {
      try {
        const result = await http<IorderInfo>('GET', `/orders/${id}/pay-params`)
        console.log('再次支付', result)
        if (result.data.status === 0) {
          Taro.requestPayment({
            timeStamp: result.data.data.miniPaymentParams.timeStamp,
            nonceStr: result.data.data.miniPaymentParams.nonceStr,
            package: result.data.data.miniPaymentParams.package,
            signType: result.data.data.miniPaymentParams.signType,
            paySign: result.data.data.miniPaymentParams.paySign,
            success: function(res) {
              console.log('订单列表支付成功', res)
              Taro.showToast({
                title: '支付成功',
                icon: 'success',
                duration: 1000
              })
              Taro.requestSubscribeMessage({
                tmplIds: result.data.data.subscribeMsgTplIds,
                success: function() {
                  Taro.reLaunch({
                    url: `/packageC/paymentSucceeded/paymentSucceeded`
                  })
                },
                fail: function() {
                  Taro.reLaunch({
                    url: `/packageC/paymentSucceeded/paymentSucceeded`
                  })
                }
              })

              // 友盟 SDK 埋点
              app.uma.trackEvent('payment', { paymentStatus: '支付成功', source, type: 'null' })
            },
            fail: function(res) {
              console.log('订单列表支付失败', res)
              Taro.showToast({
                title: '取消支付',
                icon: 'error',
                duration: 1000
              })

              // 友盟 SDK 埋点
              app.uma.trackEvent('payment', { paymentStatus: '支付失败', source, type: 'null' })
            }
          })
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 确认收货
    async confirmReceipt(id: string, type?: string) {
      try {
        const result = await http('POST', `/orders/${id}/receive`)
        console.log('确认收货', result)
        Taro.showToast({
          title: '收货成功',
          icon: 'none',
          duration: 2000
        })
        setTimeout(() => {
          if (type) {
            Taro.navigateBack({
              delta: 1
            })
          }
        }, 2000)
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 评价
    async comment(id: string, data: IcommentObject) {
      try {
        const result = await http('POST', `/orders/${id}/comments`, {
          productScore: data.productScore,
          content: data.content,
          shippingScore: data.shippingScore,
          serviceScore: data.serviceScore
        })
        console.log('评价', result)
        if (result.data.status === 0) {
          Taro.showToast({
            title: '评价成功,审核通过后将展示内容',
            icon: 'none',
            duration: 2000
          })
          setTimeout(() => {
            Taro.navigateBack({
              delta: 1
            })
          }, 2000)
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 问题反馈
    async problemFeedback(
      orderId: string,
      reason: number,
      problemDescription: string,
      pictureProof: string[],
      mobile: string
    ) {
      try {
        const result = await http('POST', '/feedback', {
          orderId,
          reason,
          problemDescription,
          pictureProof,
          mobile
        })
        console.log('问题反馈', result)
        if (result.data.status === 0) {
          Taro.showToast({
            title: '已反馈!',
            icon: 'success',
            duration: 1500
          })
          setTimeout(() => {
            Taro.navigateBack({
              delta: 2
            })
          }, 1500)
        }
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    },
    // 问题反馈上传凭证
    async credentialUpload(file) {
      try {
        const result = await http('POST', '/feedback-proof', {
          file
        })
        console.log('上传凭证', result)
      } catch (error) {
        if (error.message === 'Network Error') {
          Taro.showToast({
            title: '网络异常,请检查网络链接',
            icon: 'none',
            duration: 1500
          })
        }
      }
    }
  }
})

export default useOrderStore
