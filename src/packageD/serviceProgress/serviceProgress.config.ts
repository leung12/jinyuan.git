export default definePageConfig({
  navigationBarTitleText: '服务进度',
  navigationBarBackgroundColor: '#F1F1F1',
  onReachBottomDistance: 20,
  enablePullDownRefresh: true
})
