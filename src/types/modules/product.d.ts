/*
    商品模块
*/

// 商品详情类型
export interface IproductInfo {
  buyerList: IbuyerList
  code: string
  cover: string
  detailImages: IdetailImages
  headImagesOrNoRatioLimitImages: IheadImages
  id: number
  leaveDiscount: number
  notPayDiscount: number
  name: string
  originPrice: number
  price: number
  serviceCommitments: IserviceCommitments
  soldCount: number
  store: Istore
  specs: Ispecs
  shippingFee: number
  shareEntity: IshareEntity
  buyBtnTxt: string
  comments: IcommentsObject
  loginUserId: number
  shippingPrompt: IshippingPrompt
  discountPrompt: IdiscountPrompt
  asks: Iasks[]
  videos: string[]
  attrs: Iattrs[]
  isFromAd: boolean
  recommandProducts: IrecommandProducts[]
}

interface IrecommandProducts {
  id: string
  code: string
}
interface Iasks {
  answers: Ianswers[]
  avatar: string
  nickName: string
  question: string
  relativeTime: string
}
interface Ianswers {
  avatar: string
  content: string
  nickName: string
  productSpecTitle: string
  praiseCount: number
  relativeTime: string
}

type IdiscountPrompt = IdiscountPromptObject[]
export interface IdiscountPromptObject {
  name: string
  description: string
  type: string
  amount: number
  tips?: string
}
interface IshareEntity {
  imageUrl: string
  path: string
  title: string
}
interface serviceCommitments {
  label: string
  description: string
}

export interface Istore {
  logo: string
  name: string
  weworkCsUrl: string
  weworkCorpId: string
  businessLicenceImg: string
  detailShowHomeBtn: number
  showInfo: IshowInfo
  theme: string
  id: number
}

interface IshowInfo {
  address: string
  businessLicenceAddress: string
  businessLicenceEstablishDate: string
  businessLicenceLegalRepresentative: string
  businessLicenceRegisterAuthorities: string
  businessLicenceScope: string
  businessLicenceType: string
  businessLicenceCreditCode: string
  companyName: string
  experience: string
  location: string
  productCount: string
  saleCount: string
  socialCreditCode: string
}

interface buyerList {
  avatar: string
  content: string
  nickName: string
}

export interface specsObject {
  id: number
  name1: string
  name2?: string
  name3?: string
  originPrice: number
  price: number
  value1: string
  value2?: string
  value3?: string
}

interface IshippingPrompt {
  consignee: IProductConsignee
  expect: Iexpect
}
interface IProductConsignee {
  address: string
  cityName: string
  districtName: string
  mobile: string
  name: string
  provinceName: string
}
interface Iexpect {
  tips: string
  title: string
}

export interface Iattrs {
  name: string
  value: string
}

// 评论
interface IcommentsObject {
  count: number
  items: Icomment
}
export type Icomment = IcommentItem[]
export interface IcommentItem {
  avatar: string
  content: string
  nickName: string
  tag: string
  images: string[]
  productSpecTitle: string
  praiseCount: number
  relativeTime: string
}

// 评论预览图片对象
export interface IpreviewImagesObj {
  src: string
}

// 商品评论标签对象数组
export interface IcommentTagObjectList {
  tagName: string
  count: number
}

// 评论

export type Ivalue1List = string[]
export type IbuyerList = buyerList[]
export type IserviceCommitments = serviceCommitments[]
export type IdetailImages = string[]
export type IheadImages = string[]
export type Ispecs = specsObject[]

// 推荐商品列表
export type IrecommendProductList = IproductObj[]

interface IproductObj {
  code: string
  cover: string
  id: number
  name: string
  originPrice: number
  price: number
  serviceCommitments: IserviceCommitments
  soldCount: number
  leaveDiscount: number
  notPayDiscount: number
  shippingFee: number
  listLongImage: string
}

interface serviceCommitments {
  label: string
  description: string
}

// swiper 组件
export interface IpreviewMedia {
  url: string
  type: 'video' | 'image'
}

// 快手回传归因参数数据
// export interface IbackhaulParams {
//   callback: string
//   aid: number
//   did: number
//   cid: number
//   ksChannel?: string
// }

// 店铺二维码
export interface IstoreQrCode {
  id: number
  qrcodeDescription: string
  randomQrcodeImage: string[]
}
