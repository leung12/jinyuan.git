/*
    地址模块
*/

// 省市区地址
export type IprovincialAndUrbanAreasList = Iprovince[]

export interface Iprovince {
  id: string
  text: string
  children: IcityChildren
}

type IcityChildren = Icity[]
export interface Icity {
  id: string
  text: string
  children: IcountyChildren
}

type IcountyChildren = Icounty[]
interface Icounty {
  id: string
  text: string
}

// 微信地址本
export interface IaddressObject {
  cityName: string
  countyName: string
  detailInfo: string
  errMsg: string
  nationalCode: string
  postalCode: string
  provinceName: string
  telNumber: string
  userName: string
}
