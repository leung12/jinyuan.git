/*
    用户模块
*/

// Taro.getUserProfile 返回类型
export interface IuserProfile {
  cloudID: undefined
  encryptedData: string
  errMsg: string
  iv: string
  rawData: string
  signature: string
  userInfo: IuserInfoObject
}
interface IuserInfoObject {
  avatarUrl: string
  city: string
  country: string
  gender: number
  language: string
  nickName: string
  province: string
}

// 登录接口返回数据类型
export interface IloginInfo {
  accessToken: string
  id: number
  mobile: string
  nickName: string
  avatar: string
  province: string
  city: string
}

// 修改用户信息接口返回类型
export interface ImodifyUseInfo {
  avatar: string
  id: number
  nickName: string
}

// 绑定手机号返回的类型
export interface IbindPhone {
  avatar: string
  id: number
  mobile: string
  nickName: string
}

// 登录接口需要的 utmInfo 参数类型
export interface IutmInfoparams {
  $taroTimestamp?: number
  utm_source?: string
  utm_medium?: string
  utm_content?: string
  adId?: string
  clickId?: string
  code?: string
  affId?: string
}

// 我的页面用户信息
export interface ImyMall {
  notPayOrders: InotPayOrder[]
  user: {
    id: number
    nickName: string
    avatar: string
    mobile: string
  }
  store: IuserStore
  weworkCsUrl: string
  weworkCorpId: string
}

interface IuserStore {
  logo: string
  name: string
  weworkCorpId: string
  weworkCsUrl: string
}

// 待支付订单列表
export type InotPayOrderList = InotPayOrder[]
interface InotPayOrder {
  expireTime: number
  createTime: number
  id: string
  items: Iitem
  payAmount: number
  payTime: number
  productAmount: number
  discountAmount: number
  shippingFee: number
  transactionNo: string
  shippingName: string
  shippingTime: string
  trackingNumber: string
  status: Istatus
}

interface Istatus {
  value: string
  text: string
  extra?: IextraObject
}
interface IextraObject {
  expireTime: number
}
interface IitemsObject {
  cover: string
  id: number
  leaveDiscount: number
  notPayDiscount: number
  payPrice: number
  price: number
  productId: number
  enterDiscount: number
  discountType: string
  productName: string
  productSpecId: number
  productSpecTitle: string
  quantity: number
  productCode: string
}
type Iitem = IitemsObject[]
