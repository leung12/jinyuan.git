/*
    订单模块
*/

// 收货人信息类型
export interface Iconsignee {
  name: string
  mobile: string
  provinceId: number
  provinceName: string
  cityId: number
  cityName: string
  districtId: number
  districtName: string
  address: string
}

// 订单下单接口返回类型
export interface IorderInfo {
  miniPaymentParams: IminiPaymentParams
  orderId: string
  payType: string
  payAmount?: number
  subscribeMsgTplIds: string[]
  notPaySubscribeMsgTplIds: string[]
}

interface IminiPaymentParams {
  appId: string
  nonceStr: string
  package: string
  paySign: string
  signType: keyof Taro.requestPayment.SignType | undefined
  timeStamp: string
}

// 广告点击相关参数
export interface IadvertisementParams {
  $taroTimestamp: number
  clickId: string
  code: string
  notShare: string
  utm_content: string
  utm_medium: string
  utm_source: string
  utm_campaign: string
  traceId: string
  data: IbackhaulParams
}

export interface IbackhaulParams {
  callback: string
  aid: number | string
  did: number | string
  cid: number | string
  ksChannel?: string
  account?: number | string
}

export interface IoriginalKsParams {
  callback: string
  ksCampaignId: number
  ksChannel: string
  ksCreativeId: number
  ksUnitId: number
  ksSiteAccountId: number
}

// 订单列表类型
export type IorderList = IdataObject[]

interface IdataObject {
  createTime: number
  id: string
  items: Iitems
  payAmount: number
  payTime: number
  productAmount: number
  discountAmount: number
  status: Istatu
  shippingFee: number
  store: IorderStore
  actions: Iactions[]
  consignee: IorderConsignee
}

export interface IorderStore {
  logo: string
  name: string
  weworkCsUrl: string
  weworkCorpId: string
}

interface Iextra {
  expireTime: number
  id: number
  receiveStatus: number
  returnShippingName: string
  returnStatus: number
  returnTrackingNumber: string
  status: number
  type: number
}
interface Istatu {
  value: string
  text: string
  extra?: Iextra
}

interface IitemObject {
  cover: string
  id: number
  leaveDiscount: number
  notPayDiscount: number
  payPrice: number
  price: number
  productId: number
  enterDiscount: number
  discountType: string
  productName: string
  productSpecId: number
  productSpecTitle: string
  quantity: number
  productCode: string
}
type Iitems = IitemObject[]

// 订单详情数据
// 完成的订单详情
export interface IorderDetailObject {
  createTime: number
  id: string
  items: Iitems
  payAmount: number
  payTime: number
  productAmount: number
  discountAmount: number
  status: Istatu
  shippingFee: number
  consignee: IorderConsignee
  transactionNo: string
  shippingName: string
  shippingTime: string
  trackingNumber: string
  actions: Iactions[]
  store: IorderStore
  priceInfo: IpriceInfo[]
}

export interface IpriceInfo {
  title: string
  subTitle?: string
  amount: number
}

interface Iactions {
  action: string
  highlight: boolean
  text: string
  extra?: IactionsExtra
}
interface IactionsExtra {
  route?: string
  receiveStatus?: number
  type?: number
  payAmount?: number
}

// 订单详情的商品信息
export interface IitemObject {
  cover: string
  id: number
  leaveDiscount: number
  notPayDiscount: number
  originPrice: number
  payPrice: number
  price: number
  enterDiscount: number
  discountType: string
  productId: number
  productName: string
  productSpecId: number
  productSpecTitle: string
  quantity: number
  productCode: string
}

// 订单的收货信息
interface IorderConsignee {
  address: string
  cityName: string
  districtName: string
  mobile: string
  name: string
  provinceName: string
}

// 订单申请售后
// 后端返回数据类型
export interface IafterSale {
  afterSale: Iobject
}
interface Iobject {
  id: number
  orderId: string
}

// 申请售后请求参数对象
export interface IafterSaleObject {
  receiveStatus: number
  type: number
  reason: number
  refundAmount: number
  returnQuantity: number
  detailReason: string
  contactName: string
  contactMobile: string
  tracking_number: string
}

// 订单售后详情
export interface IafterSalesDetail {
  createTime: number
  id: number
  order: Iorder
  reason: string
  refundTime: number
  refundAmount: number
  status: number
  type: number
  detailReason: string
  receiveStatus: number
  returnStatus: number
  contactName: string
  contactMobile: string
  returnQuantity: number
  process: IprocessObj
  autoRefundTime: number
}

// IprocessObj
interface IprocessObj {
  refundAccount: string
  result: string
  steps: IstepsList
  description: Idesc
}

interface Idesc {
  subTitle: string
  title: string
}

export type IstepsList = IstepItem[]
interface IstepItem {
  name: string
  time: string
}

// Iorder
interface Iorder {
  items: IafterSalesDetailList
  shippingTime: number
}

export interface IafterSalesDetailItemObject {
  cover: string
  id: number
  leaveDiscount: number
  notPayDiscount: number
  payPrice: number
  price: number
  productId: number
  productName: string
  productSpecId: number
  productSpecTitle: string
  quantity: number
  productCode: string
}
type IafterSalesDetailList = IafterSalesDetailItemObject[]

// 提交评论
export interface IcommentObject {
  productScore: number
  content: string
  shippingScore: number
  serviceScore: number
  images: string
}

// 下单优惠对象
export type IIdiscountPromptArray = IorderdiscountPrompt[]
export interface IorderdiscountPrompt {
  type: string
  time?: number
  amount: number
  description: string
  name: string
}

// 售后地址
export interface IafterSalesAddress {
  id: number
  biz_customer_id: number
  sellerName: string
  store_id: number
  province: string
  city: string
  district: string
  detail: string
  is_default: number
  create_time: number
  update_time: number
  mobile: number
}
