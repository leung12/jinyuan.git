/*
    首页模块
*/

export type Iresource = IswiperObject[]

// 轮播图
export interface IswiperObject {
  items: IswiperArray
  source?: string
  type: string
}
interface IitemObj {
  image: string
  productId: number
  relatedType: string
  route: string
  title: string
  label?: string
  value?: string
}
export type IswiperArray = IitemObj[]

// tabs列表
export interface ItabsObject {
  type: string
  source?: string
  items: ItabsrArray
}
interface Iitem2 {
  label: string
  value: string
}
export type ItabsrArray = Iitem2[]

// 首页商品列表
export type IproductList = IproductObject[]

interface IproductObject {
  code: string
  cover: string
  id: number
  leaveDiscount: number
  notPayDiscount: number
  name: string
  originPrice: number
  price: number
  soldCount: number
  shippingFee: number
  serviceCommitments: IserviceCommitment
  listLongImage: string
}

type IserviceCommitment = serviceCommitment[]
interface serviceCommitment {
  label: string
  description: string
}

// 首页轮播随机商品图
export type IrandProduct = Iproduct[]

interface Iproduct {
  code: string
  cover: string
  id: number
  name: string
}
