/*
    反馈模块
*/

export type IfeedbackList = IfeedbackObject[]

export interface IfeedbackObject {
  conversations: Iconversations[]
  description: string
  id: number
  mobile: string
  reason: number
  status: number
  order: IorderItem
  createTime: number
}

interface Iconversations {
  content: string
  id: number
  user: string
  type: number
  createTime: number
}

interface IorderItem {
  items: IorderItemObject[]
}

interface IorderItemObject {
  cover: string
  id: number
  originPrice: number
  leaveDiscount: number
  notPayDiscount: number
  payPrice: number
  price: number
  productId: number
  productName: string
  productSpecId: number
  productSpecTitle: string
  quantity: number
  productCode: string
}
