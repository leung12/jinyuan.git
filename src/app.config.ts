export default defineAppConfig({
  pages: ['pages/index/index', 'pages/order/order', 'pages/my/my', 'pages/test/test'],
  subpackages: [
    {
      root: 'package',
      name: 'product',
      pages: ['product/detail', 'productDetail/frontPage']
    },
    {
      root: 'packageB',
      pages: [
        'comment/comment',
        'askAll/askAll',
        'askAllDetail/askAllDetail',
        'shopInfo/shopInfo',
        'shopFront/shopFront',
        'shopCard/shopCard',
        'shopDetail/shopDetail',
        'shopLicence/shopLicence'
      ]
    },
    {
      root: 'packageC',
      pages: [
        'order/order',
        'orderDetail/orderDetail',
        'afterSale/afterSale',
        'afterSaleDetail/afterSaleDetail',
        'paymentSucceeded/paymentSucceeded',
        'postComment/postComment',
        'questionOrder/questionOrder'
      ]
    },
    {
      root: 'packageD',
      pages: [
        'userAgreement/userAgreement',
        'privacyAgreement/privacyAgreement',
        'personalInfo/personalInfo',
        'complaintSuggestion/complaintSuggestion',
        'submitComplaint/submitComplaint',
        'coupon/coupon',
        'followAccount/followAccount',
        'serviceProgress/serviceProgress',
        'serviceDetail/serviceDetail',
        'userInfo/userInfo',
        'help/help',
        'consumerExplanation/consumerExplanation'
      ]
    }
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  permission: {
    'scope.userLocation': {
      desc: '你的位置信息将用于小程序位置接口的效果展示'
    }
  },
  // requiredBackgroundModes: ['audio', 'location'],
  requiredPrivateInfos: ['getLocation', 'chooseAddress', 'chooseLocation', 'choosePoi'],
  lazyCodeLoading: 'requiredComponents',
  tabBar: {
    list: [
      {
        pagePath: 'pages/index/index',
        text: '首页',
        iconPath: './assets/images/index.png',
        selectedIconPath: './assets/images/index_selected.png'
      },
      {
        pagePath: 'pages/order/order',
        text: '订单',
        iconPath: './assets/images/order.png',
        selectedIconPath: './assets/images/order_selected.png'
      },
      {
        pagePath: 'pages/my/my',
        text: '我的',
        iconPath: './assets/images/my.png',
        selectedIconPath: './assets/images/my_selected.png'
      }
      // {
      //   pagePath: 'pages/test/test',
      //   text: '测试',
      //   iconPath: './assets/images/my.png',
      //   selectedIconPath: './assets/images/my_selected.png'
      // }
    ],
    // color: "#e65137",
    selectedColor: '#fc732a'
  }
})
