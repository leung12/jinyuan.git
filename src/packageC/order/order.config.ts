export default definePageConfig({
  navigationBarTitleText: '订单',
  enablePullDownRefresh: true,
  navigationBarBackgroundColor: '#F1F1F1',
  onReachBottomDistance: 100,
  backgroundTextStyle: 'dark'
})
