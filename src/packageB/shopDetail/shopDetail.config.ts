export default definePageConfig({
  navigationBarTitleText: '店铺',
  navigationBarBackgroundColor: '#FFF',
  onReachBottomDistance: 100,
  enablePullDownRefresh: true
})
