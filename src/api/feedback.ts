import { http } from '../service/request/request'
import { IfeedbackList, IfeedbackObject } from '../types/index'

// 获取反馈列表
export function getListOfFeedback(page, limit) {
  return http<IfeedbackList>('GET', `/feedback`, {
    fields: '{id,reason,description,mobile,conversations,status,order{items}}',
    page,
    limit
  })
}

// 获取反馈详情
export function getDetailOfFeedback(id) {
  return http<IfeedbackObject>('GET', `/feedback/${id}`, {
    fields: '{id,reason,description,mobile,conversations,status,order{items},createTime}'
  })
}

// 反馈-回复
export function feedbackReply(id, content) {
  return http('POST', `/feedback-reply`, {
    id,
    content
  })
}
