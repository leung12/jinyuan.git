import { http } from '../service/request/request'
import { ImyMall, ImodifyUseInfo } from '../types/index'

// 获取用户信息
export function getUserInfo() {
  return http<ImyMall>('GET', '/user/my-mall', {
    fields:
      '{user{id,nickName,avatar,mobile},weworkCsUrl,weworkCorpId,notPayOrders{id,createTime,expireTime,payTime,productAmount,payAmount,discountAmount,status,items,shippingFee,transactionNo,consignee,shippingName,shippingTime,trackingNumber},store}'
  })
}

// 修改用户信息
export function modifyUserInfo(nickName: string, avatar: string) {
  return http<ImodifyUseInfo>('PUT', '/user/modify-info', {
    nickName,
    avatar
  })
}
