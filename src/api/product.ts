import { http } from '../service/request/request'
import { IrecommendProductList, IproductInfo, IproductList, Iresource } from '../types/index'

// 获取商品详情
export function getProductDetail(code, utmInfo) {
  return http<IproductInfo>('GET', `/products/${code}`, {
    fields:
      '{id,code,name,cover,headImagesOrNoRatioLimitImages,detailImages,price,originPrice,serviceCommitments,soldCount,leaveDiscount,notPayDiscount,buyerList,store,specs,shippingFee,shareEntity,buyBtnTxt,comments,loginUserId,shippingPrompt,discountPrompt,asks,videos,attrs,isFromAd,recommandProducts{id,code}}',
    utmInfo
  })
}

// 获取推荐商品
export function getRecommendedProducts(page, limit, storeId) {
  return http<IrecommendProductList>('GET', '/product/my-recommend', {
    fields: '{id,code,name,cover,price,originPrice,serviceCommitments,soldCount,listLongImage}',
    page,
    limit,
    storeId
  })
}

// 商品对应的推荐商品
export function getRecommendedProducts_product(page, limit, productId) {
  return http<IproductList>('GET', '/product/recommend', {
    fields: '{id,code,name,cover,price,originPrice,soldCount,shippingFee,listLongImage}',
    limit,
    page,
    productId
  })
}

// 获取商品列表
export function getProduct(limit, page, value) {
  return http<IproductList>('GET', '/products', {
    fields: '{id,code,name,cover,price,originPrice,soldCount,listLongImage}',
    limit,
    page,
    filter: `keyword,${encodeURIComponent(value)}`
  })
}

// 获取商品列表(storeId)
export function getProduct_storeId(limit, page, value, storeId) {
  return http<IproductList>('GET', '/products', {
    fields: '{id,code,name,cover,price,originPrice,soldCount,listLongImage}',
    limit,
    page,
    filter: `keyword,${encodeURIComponent(value)}`,
    storeId
  })
}

// 获取商品列表 -- 仅获取 id
export function getProduct_id(limit, page, value) {
  return http<IproductList>('GET', '/products', {
    fields: '{id}',
    limit,
    page,
    filter: `keyword,${encodeURIComponent(value)}`
  })
}

// 获取商品列表(storeId) -- 仅获取 id
export function getProduct_storeId_id(limit, page, value, storeId) {
  return http<IproductList>('GET', '/products', {
    fields: '{id}',
    limit,
    page,
    filter: `keyword,${encodeURIComponent(value)}`,
    storeId
  })
}

// 首页资源位
export function getHomeResources() {
  return http<Iresource>('GET', '/resource-slot/home')
}
