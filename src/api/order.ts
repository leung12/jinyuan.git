import { http } from '../service/request/request'
import {
  IorderList,
  IorderDetailObject,
  IafterSaleObject,
  IafterSale,
  IafterSalesDetail,
  IafterSalesAddress
} from '../types/index'

// 获取订单列表
export function getOrderList(status: string, limit: number) {
  return http<IorderList>('GET', '/orders', {
    fields:
      '{id,createTime,payTime,productAmount,discountAmount,payAmount,status,items,shippingFee,store,actions,consignee}',
    page: '1',
    limit: `${limit}`,
    status
  })
}

// 获取订单详情
export function getOrderDetail(id) {
  return http<IorderDetailObject>('GET', `/orders/${id}`, {
    fields:
      '{id,createTime,payTime,productAmount,payAmount,discountAmount,status,items,shippingFee,transactionNo,consignee,shippingName,shippingTime,trackingNumber,actions,store,priceInfo}',
    id
  })
}

// 申请售后
export function applyAfterSale(id: string, params: IafterSaleObject) {
  return http<IafterSale>('POST', `/orders/${id}/after-sales`, {
    ...params
  })
}

// 获取售后详情
export function getAfterSalesDetail(id: string) {
  return http<IafterSalesDetail>('GET', `/orders/${id}/latest-after-sale`, {
    fields:
      '{id,type,reason,createTime,status,refundTime,refundAmount,order{items,shippingTime},detailReason,receiveStatus,contactName,contactMobile,returnQuantity,process,returnStatus,autoRefundTime}'
  })
}

// 获取商品售后地址
export function getProductAfterSaleAddress(id: number) {
  return http<IafterSalesAddress>('GET', `/products/${id}/after-sale-address`, {
    fields: '{id,province,city,district,detail,mobile,sellerName}'
  })
}

// 售后提交退货物流单号
export function submitReturnTrackingNumber(
  id: number,
  returnShippingName: string,
  returnTrackingNumber: string
) {
  return http('POST', `/after-sales/${id}/submit-return-tracking-number`, {
    returnShippingName,
    returnTrackingNumber
  })
}

// 撤回售后申请
export function afterSaleApplyWithdraw(orderId: string, afterSaleId: number, cancelStep: number) {
  return http('POST', `/orders/${orderId}/after-sales/${afterSaleId}`, {
    cancelStep
  })
}
