import { createApp, ref } from 'vue'
import { createPinia } from 'pinia'
import uma from 'umtrack-wx'
import Taro, { setGlobalDataPlugin } from '@tarojs/taro'
import {
  Cell,
  Button,
  Icon,
  Input,
  Popup,
  OverLay,
  InputNumber,
  Rate,
  Dialog,
  CellGroup,
  Cascader,
  Sticky,
  Empty,
  Skeleton,
  Avatar,
  Steps,
  Step,
  CountDown,
  SearchBar,
  Progress,
  Uploader,
  NoticeBar
} from '@nutui/nutui-taro'

uma.init({
  appKey: '6442374f4c2b215d804076df',
  useOpenid: true,
  autoGetOpenid: true,
  debug: true,
  uploadUserInfo: true
})

import './app.scss'
import './assets/icons/iconfont.scss'

/* 实例化pinia */
const pinia = createPinia()

const App = createApp({
  onShow(options) {
    // 本地储存场景值
    Taro.setStorageSync('scene', options.scene)

    // 获取底部安全区域top值
    Taro.getSystemInfo({
      success: res => {
        safeAreaTop.value = res.safeArea?.top
      }
    })

    // 本地存储flag判断用户是否第一次登录小程序
    Taro.setStorageSync('firstLogin', true)
  },
  onLaunch() {},

  onHide() {
    uma.trackEvent('close_the_miniprogram', { source: Taro.getStorageSync('pageWhenLeaving') })
  }
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
})

/* 设置全局变量 */
const safeAreaTop = ref() // 安全区域top值

App.use(setGlobalDataPlugin, {
  globalSafeAreaTop: safeAreaTop,
  // 全局挂载友盟 SDK 实例
  uma: uma
})

App.use(Button)
  .use(Cell)
  .use(Icon)
  .use(Input)
  .use(Popup)
  .use(OverLay)
  .use(InputNumber)
  .use(Rate)
  .use(Dialog)
  .use(CellGroup)
  .use(Cascader)
  .use(Sticky)
  .use(Empty)
  .use(Avatar)
  .use(Skeleton)
  .use(Steps)
  .use(Step)
  .use(CountDown)
  .use(SearchBar)
  .use(Progress)
  .use(Uploader)
  .use(NoticeBar)
  .use(pinia)

export default App
