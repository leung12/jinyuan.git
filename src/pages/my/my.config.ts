export default definePageConfig({
  navigationBarTitleText: '我的',
  backgroundTextStyle: 'dark',
  navigationStyle: 'custom',
  onReachBottomDistance: 10,
  enablePullDownRefresh: true
})
