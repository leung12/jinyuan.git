export default definePageConfig({
  navigationBarTitleText: '推荐',
  navigationBarBackgroundColor: '#ffffff',
  navigationBarTextStyle: 'black',
  backgroundTextStyle: 'dark',
  navigationStyle: 'custom'
})
